import numpy as np 
import theano.tensor as T 
import theano, lasagne 
from collections import OrderedDict
from learned_norm_pool import * 

input_var = T.tensor4('inputs')
net = OrderedDict()
net['input'] = lasagne.layers.InputLayer(input_var=input_var, shape=(None,1,4,4))
net['lp_pool'] = LearnedNormPoolingLayer2D(incoming=net['input'], pool_size=(2,2), stride=None, pad=(0,0), ignore_border=True)
lp_pool_out = lasagne.layers.get_output(net['lp_pool'])
get_out = theano.function([input_var], lp_pool_out)

x = np.array([[[[ 0.94291383,  0.18401679,  0.51992327,  0.82380855],
         [ 0.04015098,  0.67312306,  0.88362986,  0.28000858],
         [ 0.65803468,  0.41766393,  0.34574455,  0.54855853],
         [ 0.25850043,  0.25230953,  0.9074778 ,  0.08102223]]],


       [[[ 0.11208387,  0.55136049,  0.85972995,  0.76930511],
         [ 0.55498081,  0.94687557,  0.61396831,  0.13999087],
         [ 0.4100512 ,  0.89124638,  0.54933131,  0.8762542 ],
         [ 0.22900669,  0.87045544,  0.25875473,  0.6237554 ]]],


       [[[ 0.91968632,  0.18639748,  0.11765624,  0.7880885 ],
         [ 0.20654008,  0.58432853,  0.28704232,  0.61905396],
         [ 0.89608079,  0.71086782,  0.21690273,  0.95215571],
         [ 0.70398444,  0.27703774,  0.50160855,  0.29425082]]]], dtype=theano.config.floatX)
y = get_out(x)

print x 
print y