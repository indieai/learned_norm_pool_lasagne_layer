import lasagne, theano, numpy 
import theano.tensor as T
from theano import gof, Op, tensor, Variable, Apply
from lasagne.layers import Layer 
from lasagne.utils import as_tuple
from lasagne.layers.pool import pool_output_length
from six.moves import xrange
import six.moves.builtins as builtins

class LpPoolLayer(Layer):
	def __init__(self, incoming, pool_size, stride, pad, ignore_border, order=lasagne.init.Constant(val=2.0), **kwargs):

		super(LpPoolLayer, self).__init__(incoming, **kwargs)
		if len(self.input_shape) != 4:
			raise ValueError("Tried to create a 2D pooling layer with "
							 "input shape %r. Expected 4 input dimensions "
							 "(batchsize, channels, 2 spatial dimensions)."
							 % (self.input_shape,))
		if stride is None:
			self.stride = self.pool_size
		else:
			self.stride = as_tuple(stride, 2)
		self.pool_size = as_tuple(pool_size, 2)
		self.pad = as_tuple(pad, 2)
		self.ignore_border = ignore_border
		self.order = self.add_param(order, (1,), name='lp_order', trainable=False, regularizable=False)

	def get_output_shape_for(self, input_shape):
		output_shape = list(input_shape)
		output_shape[2] = pool_output_length(self.input_shape[2],
											 pool_size=self.pool_size[0],
											 stride=self.stride[0],
											 pad=self.pad[0],
											 ignore_border=self.ignore_border,
											 )
		output_shape[3] = pool_output_length(self.input_shape[3],
											 pool_size=self.pool_size[1],
											 stride=self.stride[1],
											 pad=self.pad[1],
											 ignore_border=self.ignore_border,
											 )
		return tuple(output_shape)

	def get_output_for(self, input, **kwargs):
		pooled = lp_pool(	input=input,
							ds=self.pool_size,
							ignore_border=self.ignore_border,
							st=self.stride,
							padding=self.pad,
							lp_order=self.order)

		return pooled

def lp_pool(input, ds, lp_order, ignore_border=None, st=None, padding=(0, 0)):
	# Currently leaving out a bunch of raise value error stuff. Cutting right to the chase
	op = LpDownSample(ds=ds, ignore_border=ignore_border, st=st, padding=padding)
	output = op(input, lp_order)
	return output

class LpDownSample(Op):

	@staticmethod
	def out_shape(imgshape, ds, ignore_border=False, st=None, padding=(0, 0)):
		if len(imgshape) < 2:
			raise TypeError('imgshape must have at least two elements '
							'(rows, cols)')

		if st is None:
			st = ds
		r, c = imgshape[-2:]
		r += padding[0] * 2
		c += padding[1] * 2

		if ignore_border:
			if ds[0] == st[0]:
				nr = r // st[0]
			else:
				out_r = (r - ds[0]) // st[0] + 1
				if isinstance(r, theano.Variable):
					nr = tensor.maximum(out_r, 0)
				else:
					nr = numpy.maximum(out_r, 0)

			if ds[1] == st[1]:
				nc = c // st[1]
			else:
				out_c = (c - ds[1]) // st[1] + 1
				if isinstance(c, theano.Variable):
					nc = tensor.maximum(out_c, 0)
				else:
					nc = numpy.maximum(out_c, 0)
		else:
			if isinstance(r, theano.Variable):
				nr = tensor.switch(tensor.ge(st[0], ds[0]),
								   (r - 1) // st[0] + 1,
								   tensor.maximum(0, (r - 1 - ds[0]) //
												  st[0] + 1) + 1)
			elif st[0] >= ds[0]:
				nr = (r - 1) // st[0] + 1
			else:
				nr = max(0, (r - 1 - ds[0]) // st[0] + 1) + 1

			if isinstance(c, theano.Variable):
				nc = tensor.switch(tensor.ge(st[1], ds[1]),
								   (c - 1) // st[1] + 1,
								   tensor.maximum(0, (c - 1 - ds[1]) //
												  st[1] + 1) + 1)
			elif st[1] >= ds[1]:
				nc = (c - 1) // st[1] + 1
			else:
				nc = max(0, (c - 1 - ds[1]) // st[1] + 1) + 1

		rval = list(imgshape[:-2]) + [nr, nc]
		return rval

	def __init__(self, ds, ignore_border=False, st=None, padding=(0, 0)):
		self.ds = tuple(ds)
		if not all([isinstance(d, int) for d in ds]):
			raise ValueError(
				"LpDownSample parameters must be ints."
				" Got %s" % str(ds))
		if st is None:
			st = ds
		assert isinstance(st, (tuple, list))
		self.st = tuple(st)
		self.ignore_border = ignore_border
		self.padding = tuple(padding)
		if self.padding != (0, 0) and not ignore_border:
			raise NotImplementedError(
				'padding works only with ignore_border=True')
		if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
			raise NotImplementedError(
				'padding_h and padding_w must be smaller than strides')
		# self.mode = 'max'
		self.mode = 'lp'

	def make_node(self, x, lp_order):
		if x.type.ndim != 4:
			raise TypeError()
		# TODO: consider restricting the dtype?
		x = tensor.as_tensor_variable(x)
		# If the input shape are broadcastable we can have 0 in the output shape
		lp_order = tensor.as_tensor_variable(lp_order)
		broad = x.broadcastable[:2] + (False, False)
		out = tensor.TensorType(x.dtype, broad)
		return gof.Apply(self, [x, lp_order], [out()])

	def perform(self, node, inp, out):
		x, lp_order = inp[0], inp[1]
		z = out[0]

		z_shape = self.out_shape(x.shape, self.ds, self.ignore_border, self.st,
								 self.padding)

		z[0] = numpy.zeros(z_shape) 
		zz = z[0]
		pr = zz.shape[-2]
		# number of pooling output cols
		pc = zz.shape[-1]
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pad_h = self.padding[0]
		pad_w = self.padding[1]
		img_rows = x.shape[-2] + 2 * pad_h
		img_cols = x.shape[-1] + 2 * pad_w

		# pad the image
		if self.padding != (0, 0):
			y = numpy.zeros(
				(x.shape[0], x.shape[1], img_rows, img_cols),
				dtype=x.dtype)
			y[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)] = x
		else:
			y = x

		for n in xrange(x.shape[0]):
			for k in xrange(x.shape[1]):
				for r in xrange(pr):
					row_st = r * st0
					row_end = builtins.min(row_st + ds0, img_rows)
					# if not inc_pad:
					# 	row_st = builtins.max(row_st, self.padding[0])
					# 	row_end = builtins.min(row_end, x.shape[-2] + pad_h)
					for c in xrange(pc):
						col_st = c * st1
						col_end = builtins.min(col_st + ds1, img_cols)
						# if not inc_pad:
						# 	col_st = builtins.max(col_st, self.padding[1])
						# 	col_end = builtins.min(col_end,
						# 						   x.shape[-1] + pad_w)

						# Compute the actual Lp norm
						# zz[n, k, r, c] = numpy.sum(numpy.abs(y[
						# 	n, k, row_st:row_end, col_st:col_end])**(lp_order))**(1.0/lp_order)

						# Compute an averaged version of the Lp norm
						zz[n, k, r, c] = numpy.average(numpy.abs(y[
							n, k, row_st:row_end, col_st:col_end])**(lp_order))**(1.0/lp_order)

		z[0]=zz.astype(theano.config.floatX) # 1/19/16: fix my code so I don't have to recast anything 

	def grad(self, inp, grads):
		x, lp_order = inp[0], inp[1]
		lp_pool_out = self(x, lp_order)
		# gz = grads[0] # Doing it this way gz has ndim = 3, not 4.
		gz, = grads # This way gz has dim 4, which is correct.
		# print type(gz)
		# print gz.ndim
		return [LpPoolGrad(	self.ds,
							ignore_border=self.ignore_border,
							st=self.st, padding=self.padding)(x, lp_pool_out, lp_order, gz), theano.tensor.zeros_like(lp_order)]

	def c_headers(self):
		# return [<'algorithm'>]
		return ['<algorithm>']

	def c_code(self, node, name, inp, out, sub):
		if self.mode not in ('max', 'sum', 'average_exc_pad', 'average_inc_pad', 'lp'):
			raise theano.gof.utils.MethodNotDefined()
		x, lp_order, = inp
		z, = out
		fail = sub['fail']
		ignore_border = int(self.ignore_border)
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		ccode = """
		int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
		// dtype_%(lp_order)s lp_order;
		// lp_order = ((dtype_%(lp_order)s*)(PyArray_DATA(%(lp_order)s)));
		dtype_%(lp_order)s * lp_order = (dtype_%(lp_order)s*)PyArray_DATA(%(lp_order)s);
		//lp_order += 1;

		int z_r, z_c; // shape of the output
		int r, c; // shape of the padded_input
		if(PyArray_NDIM(%(x)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "x must be a 4d ndarray");
			%(fail)s;
		}
		r = PyArray_DIMS(%(x)s)[2];
		c = PyArray_DIMS(%(x)s)[3];
		r += %(pd0)s * 2;
		c += %(pd1)s * 2;

		if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
			{
			  PyErr_SetString(PyExc_ValueError,
				"padding must be (0,0) when ignore border is False");
			  %(fail)s;
			}
		if (%(ignore_border)s)
		{

			// '/' in C is different from '/' in python
			if (r - %(ds0)s < 0)
			{
			  z_r = 0;
			}
			else
			{
			  z_r = (r - %(ds0)s) / %(st0)s + 1;
			}
			if (c - %(ds1)s < 0)
			{
			  z_c = 0;
			}
			else
			{
			  z_c = (c - %(ds1)s) / %(st1)s + 1;
			}
		}
		else
		{
			// decide how many rows the output has
			if (%(st0)s >= %(ds0)s)
			{
				z_r = (r - 1) / %(st0)s + 1;
			}
			else
			{
				z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
			}
			// decide how many columns the output has
			if (%(st1)s >= %(ds1)s)
			{
				z_c = (c - 1) / %(st1)s + 1;
			}
			else
			{
				z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
			}
		}
		// memory allocation of z if necessary
		if ((!%(z)s)
		  || *PyArray_DIMS(%(z)s)!=4
		  ||(PyArray_DIMS(%(z)s)[0] != PyArray_DIMS(%(x)s)[0])
		  ||(PyArray_DIMS(%(z)s)[1] != PyArray_DIMS(%(x)s)[1])
		  ||(PyArray_DIMS(%(z)s)[2] != z_r)
		  ||(PyArray_DIMS(%(z)s)[3] != z_c)
		  )
		{
		  if (%(z)s) Py_XDECREF(%(z)s);
		  npy_intp dims[4] = {0,0,0,0};
		  dims[0]=PyArray_DIMS(%(x)s)[0];
		  dims[1]=PyArray_DIMS(%(x)s)[1];
		  dims[2]=z_r;
		  dims[3]=z_c;
		  //TODO: zeros not necessary
		  %(z)s = (PyArrayObject*) PyArray_ZEROS(4, dims, typenum,0);
		}

		// used for indexing a pool region inside the input
		int r_st, r_end, c_st, c_end;
		dtype_%(x)s collector; // temp var for the value in a region
		if (z_r && z_c)
		{
			for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
			  for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
				for(int i=0; i< z_r; i++){
				  r_st = i * %(st0)s;
				  r_end = r_st + %(ds0)s;
				  // skip the padding
				  r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
				  r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
				  // from padded_img space to img space
				  r_st -= %(pd0)s;
				  r_end -= %(pd0)s;

				  // handle the case where no padding, ignore border is True
				  if (%(ignore_border)s)
				  {
					r_end = r_end > r ? r : r_end;
				  }
				  for(int j=0; j<z_c; j++){
					c_st = j * %(st1)s;
					c_end = c_st + %(ds1)s;
					// skip the padding
					c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
					c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
					dtype_%(z)s * z = (
						  (dtype_%(z)s*)(PyArray_GETPTR4(%(z)s, b, k, i, j)));
					// change coordinates from padding_img space into img space
					c_st -= %(pd1)s;
					c_end -= %(pd1)s;
					// handle the case where no padding, ignore border is True
					if (%(ignore_border)s)
					{
					  c_end = c_end > c ? c : c_end;
					}
		"""
		if self.mode == 'lp':
			ccode += """
					// use the first element as the initial value of collector
					// collector = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,r_st,c_st)))[0];
					// double collector = 0;
					collector = 0.0f;
					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
						 // collector = (a > collector) ? pow(a,2) : collector;
						 // collector += pow(abs(a), lp_order);
						 // collector += pow(a, 2.0);
						 collector += powf(fabs(a), *lp_order);
					  }
					}
					// z[0] = pow(collector, 1.0/*lp_order);

					// Need to do the average, i.e. collector /= (z_r*z_c)
					collector = collector / ( (float) (z_r * z_c) );
					z[0] = powf(collector, 1.0/(*lp_order) );
			"""
		if self.mode == 'max':
			ccode += """
					// use the first element as the initial value of collector
					collector = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,r_st,c_st)))[0];
					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
						collector = (a > collector) ? a : collector;
					  }
					}
					z[0] = collector;
			"""
		elif self.mode in ('sum', 'average_exc_pad', 'average_inc_pad'):
			ccode += """
					// initialize the sum at zero
					collector = ((dtype_%(x)s)(0));
					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
						collector += a;
					  }
					}
			"""
			if self.mode == "sum":
				ccode += """
					z[0] = collector;
				"""
			elif self.mode == 'average_inc_pad' and self.ignore_border:
				ccode += """
					z[0] = collector / (%(ds0)s * %(ds1)s);
				"""
			else:
				ccode += """
					z[0] = collector / ((r_end-r_st)*(c_end-c_st));
				"""
		ccode += """
				  }
				}
			  }
			}
		}
		"""
		return ccode % locals()


class PoolGrad(Op):
	__props__ = ('ds', 'ignore_border', 'st', 'padding')

	@staticmethod
	def out_shape(imgshape, ds, ignore_border=False, st=None, padding=(0, 0)):
		"""Return the shape of the output from this op, for input of given
		shape and flags.

		:param imgshape: the shape of a tensor of images. The last two elements
			are interpreted as the number of rows, and the number of cols.
		:type imgshape: tuple, list, or similar of integer or
			scalar Theano variable.

		:param ds: downsample factor over rows and columns
				   this parameter indicates the size of the pooling region
		:type ds: list or tuple of two ints

		:param st: the stride size. This is the distance between the pooling
				   regions. If it's set to None, in which case it equlas ds.
		:type st: list or tuple of two ints

		:param ignore_border: if ds doesn't divide imgshape, do we include an
			extra row/col of partial downsampling (False) or ignore it (True).
		:type ignore_border: bool

		:param padding: (pad_h, pad_w), pad zeros to extend beyond four borders
			of the images, pad_h is the size of the top and bottom margins,
			and pad_w is the size of the left and right margins.
		:type padding: tuple of two ints

		:rtype: list
		:returns: the shape of the output from this op, for input of given
			shape.  This will have the same length as imgshape, but with last
			two elements reduced as per the downsampling & ignore_border flags.
		"""
		if len(imgshape) < 2:
			raise TypeError('imgshape must have at least two elements '
							'(rows, cols)')

		if st is None:
			st = ds
		r, c = imgshape[-2:]
		r += padding[0] * 2
		c += padding[1] * 2

		if ignore_border:
			out_r = (r - ds[0]) // st[0] + 1
			out_c = (c - ds[1]) // st[1] + 1
			if isinstance(r, theano.Variable):
				nr = tensor.maximum(out_r, 0)
			else:
				nr = numpy.maximum(out_r, 0)
			if isinstance(c, theano.Variable):
				nc = tensor.maximum(out_c, 0)
			else:
				nc = numpy.maximum(out_c, 0)
		else:
			if isinstance(r, theano.Variable):
				nr = tensor.switch(tensor.ge(st[0], ds[0]),
								   (r - 1) // st[0] + 1,
								   tensor.maximum(0, (r - 1 - ds[0]) //
												  st[0] + 1) + 1)
			elif st[0] >= ds[0]:
				nr = (r - 1) // st[0] + 1
			else:
				nr = max(0, (r - 1 - ds[0]) // st[0] + 1) + 1

			if isinstance(c, theano.Variable):
				nc = tensor.switch(tensor.ge(st[1], ds[1]),
								   (c - 1) // st[1] + 1,
								   tensor.maximum(0, (c - 1 - ds[1]) //
												  st[1] + 1) + 1)
			elif st[1] >= ds[1]:
				nc = (c - 1) // st[1] + 1
			else:
				nc = max(0, (c - 1 - ds[1]) // st[1] + 1) + 1

		rval = list(imgshape[:-2]) + [nr, nc]
		return rval

	def __init__(self, ds, ignore_border, st=None, padding=(0, 0)):
		self.ds = tuple(ds)
		self.ignore_border = ignore_border
		if st is None:
			st = ds
		self.st = tuple(st)
		self.padding = tuple(padding)


	def infer_shape(self, node, in_shapes):
		return [in_shapes[0]]


class LpPoolGrad(PoolGrad):

	def __init__(self, ds, ignore_border, st=None, padding=(0, 0)):
		PoolGrad.__init__(self, ds, ignore_border, st, padding)
		self.mode='lp'

	def make_node(self, x, lp_out, lp_order, gz):
		# make_node should only be called by the grad function of
		# DownsampleFactorMax, so these asserts should not fail.
		assert isinstance(x, Variable) and x.ndim == 4
		assert isinstance(lp_out, Variable) and lp_out.ndim == 4
		assert isinstance(gz, Variable) and gz.ndim == 4
		x = tensor.as_tensor_variable(x)
		lp_out = tensor.as_tensor_variable(lp_out)
		lp_order = tensor.as_tensor_variable(lp_order)
		gz = tensor.as_tensor_variable(gz)

		return Apply(self, [x, lp_out, lp_order, gz], [x.type()])

	def perform(self, node, inp, out):
		x, lp_out, lp_order, gz = inp[0], inp[1], inp[2], inp[3]
		# print lp_order
		# print "x shape " + str(x.shape)
		# print "gz shape " + str(gz.shape)
		# gzz = gz[0]
		# print "gzz shape " + str(gzz.shape)
		gx_stg = out[0]
		# number of pooling output rows
		pr = lp_out.shape[-2]
		# number of pooling output cols
		pc = lp_out.shape[-1]
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pad_h = self.padding[0]
		pad_w = self.padding[1]
		img_rows = x.shape[-2] + 2 * pad_h
		img_cols = x.shape[-1] + 2 * pad_w

		# pad the image
		if self.padding != (0, 0):
			y = numpy.zeros(
				(x.shape[0], x.shape[1], img_rows, img_cols),
				dtype=x.dtype)
			y[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)] = x
		else:
			y = x
		gx = numpy.zeros_like(y)
		for n in xrange(x.shape[0]):
			for k in xrange(x.shape[1]):
				for r in xrange(pr):
					row_st = builtins.max(r * st0, self.padding[0])
					row_end = builtins.min(row_st + ds0, img_rows)
					for c in xrange(pc):
						col_st = builtins.max(c * st1, self.padding[1])
						col_end = builtins.min(col_st + ds1, img_cols)
						for row_ind in xrange(row_st, row_end):
							for col_ind in xrange(col_st, col_end):
								gx[n, k, row_ind, col_ind] += (1.0/(pr*pc))*gz[n, k, r, c]*lp_out[n, k, r, c]**(lp_order-1)*numpy.sign(y[n, k, row_ind, col_ind])*numpy.abs(y[n, k, row_ind, col_ind])**(lp_order-1)

		# unpad the image
		gx = gx[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)]
		gx_stg[0] = gx

	def c_headers(self):
		# return [<'algorithm'>]
		return ['<algorithm>']

	def c_code(self, node, name, inp, out, sub):
		assert self.mode == 'lp'
		x, z, lp_order, gz = inp
		# z = lp_out here 
		gx, = out
		fail = sub['fail']
		ignore_border = int(self.ignore_border)
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		return """
		// sanity checks
		int x_typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
		int z_typenum = PyArray_ObjectType((PyObject*)%(z)s, 0);
		int gz_typenum = PyArray_ObjectType((PyObject*)%(gz)s, 0);

		// Create lp_order variable: 
		dtype_%(lp_order)s * lp_order = (dtype_%(lp_order)s*)PyArray_DATA(%(lp_order)s);

		if ((x_typenum != z_typenum) || (x_typenum != gz_typenum))
		{
			PyErr_SetString(PyExc_ValueError, "input types must all match");
			%(fail)s;
		}
		if(PyArray_NDIM(%(x)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "x must be a 4d ndarray");
			%(fail)s;
		}
		if(PyArray_NDIM(%(z)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "z must be a 4d ndarray");
			%(fail)s;
		}
		if(PyArray_NDIM(%(gz)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "gz must be a 4d ndarray");
			%(fail)s;
		}

		int z_r, z_c;
		z_r = PyArray_DIMS(%(z)s)[2];
		z_c = PyArray_DIMS(%(z)s)[3];

		int r, c; // shape of the padded_input
		r = PyArray_DIMS(%(x)s)[2];
		c = PyArray_DIMS(%(x)s)[3];
		r += %(pd0)s * 2;
		c += %(pd1)s * 2;

		// allocating memory for gx
		if ((!%(gx)s)
		  || !PyArray_ISCONTIGUOUS(%(gx)s)
		  || *PyArray_DIMS(%(gx)s)!=4
		  ||(PyArray_DIMS(%(gx)s)[0] != PyArray_DIMS(%(x)s)[0])
		  ||(PyArray_DIMS(%(gx)s)[1] != PyArray_DIMS(%(x)s)[1])
		  ||(PyArray_DIMS(%(gx)s)[2] != PyArray_DIMS(%(x)s)[2])
		  ||(PyArray_DIMS(%(gx)s)[3] != PyArray_DIMS(%(x)s)[3])
		  )
		{
		  Py_XDECREF(%(gx)s);
		  %(gx)s = (PyArrayObject*) PyArray_ZEROS(4, PyArray_DIMS(%(x)s), x_typenum,0);
		}
		else {
		  PyArray_FILLWBYTE(%(gx)s, 0);
		}
		int r_st, r_end, c_st, c_end; // used to index into the input img x

		// Eliminate the maximum varaible:
		/* dtype_%(z)s maximum; // temp var for maximum value in a region */

		if (z_r && z_c)
		{
			for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
			  for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
				for(int i=0; i< z_r; i++){
				  r_st = i * %(st0)s;
				  r_end = r_st + %(ds0)s;
				  // skip the padding
				  r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
				  r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
				  // from padded_img space to img space
				  r_st -= %(pd0)s;
				  r_end -= %(pd0)s;

				  for(int j=0; j<z_c; j++){
					c_st = j * %(st1)s;
					c_end = c_st + %(ds1)s;
					// skip the padding
					c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
					c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;

					// change coordinates from padding_img space into img space
					c_st -= %(pd1)s;
					c_end -= %(pd1)s;

					/* 
					// the maximum value
					maximum = ((dtype_%(z)s*)(PyArray_GETPTR4(%(z)s,b,k,i,j)))[0];
					*/ 

					/* Max interpretation:
					// the gradient corresponding to this maximum value in z
					*/ 

					// New Lp pool interpretation
					// the gradient corresponding to this value in z
					dtype_%(gz)s * gz = (
						  (dtype_%(gz)s*)(PyArray_GETPTR4(%(gz)s, b, k, i, j)));
					dtype_%(z)s z = ((dtype_%(z)s*)(PyArray_GETPTR4(%(z)s, b, k, i, j)))[0];
					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];
						dtype_%(gx)s * gx = (
						  (dtype_%(gx)s*)(PyArray_GETPTR4(%(gx)s, b, k, m, n)));
						/*if (a == maximum){
						  gx[0] = gx[0] + gz[0];
						} */

						// gx[0] = gx[0] + gz[0]*powf(a,*lp_order-1); // Works

						// Lp pooling with averagaging: 
						gx[0] = gx[0] + (1/((float)(z_r * z_c)))*gz[0]*powf(z, *lp_order-1)*copysignf(1.0, a)*powf(fabs(a), *lp_order-1);
					  }
					}
				  }
				}
			  }
			}

		}
		""" % locals()

	# def grad(self, inp, grads):
	# 	x, lp_out, lp_order, gz = inp[0], inp[1], inp[2], inp[3]
	# 	ggx = grads[0]
	# 	return [theano.tensor.zeros_like(x),
	# 			theano.tensor.zeros_like(lp_out),
	# 			theano.tensor.zeros_like(lp_order),
	# 			theano.tensor.zeros_like(ggx)]

if __name__ == '__main__':
	pass 

	# Test the pooling

	input_var = T.tensor4('input_var')
	lp_order = T.fscalar('lp_order')
	op = LpDownSample(ds=(2,2), st=(2,2))

	pooled = op(input_var, lp_order)
	get_pooled = theano.function([input_var, lp_order], pooled)
	x = numpy.arange(16).reshape(1,1,4,4).astype(theano.config.floatX)
	# x[0,0, 1,0] *= -1
	y = get_pooled(x, 2.0)
	print x
	print y

	# Test the backprop
	gz = T.tensor4('gz')
	gx = op.grad(inp=(input_var, lp_order), grads=(gz))
	get_gx = theano.function([input_var, lp_order, gz], gx)

	gzz = numpy.array([.1,.2,.3,.4]).astype(theano.config.floatX).reshape(1,1,2,2)
	print gzz.shape
	gx_test, glp_order = get_gx(x, 2.0, gzz)
	print gx_test