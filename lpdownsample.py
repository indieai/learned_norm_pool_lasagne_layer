"""
Ops for Learned Lp Norm Pooling. 

"""
# from __future__ import print_function
from six.moves import xrange
import six.moves.builtins as builtins
import warnings
import numpy
import theano
from theano import gof, Op, tensor, Variable, Apply
from theano.tensor.opt import register_canonicalize

def sigmoid(x):
	""" Takes as input a NumPy array and returns a NumPy array of the same 
		shape. Uses a trick to avoid numerically overflow when the input x is 
		has entries that are either too large (+infinity) or too negative 
		(-infinity). 

	Parameters
	----------
	x : NumPy array type. 
		Input to the sigmoid. Sigmoid to be applied element wise. 

	"""
	if x >= 0:
		z = numpy.exp(-x)
		return 1 / (1 + z)
	else:
		z = numpy.exp(x)
		return z / (1 + z)
		
def lp_pool_func(input, power, center):
	""" Takes as input a 2D spatial pooling region of a 4D input tensor, an Lp 
		order and the center of an Lp pooling unit and returns the Lp pooling 
		units activation value.

		Returns the value z where:

		z = ((1/(ds[0]*ds[1]))*\sum_{i = 1}^ds[0] \sum_{j=1}^ds[1] |input[i,j]-center[i,j]|^power)^(1/power),
		
		where ds is a 2-tuple (ds[0], ds[1]) representing the spatial pooling 
		size of the Lp pooling unit.

	Parameters
	----------
	input : NumPy array type 
		Currently we only support input of shape 2D (ds[0], ds[1]).

	power :	float type 
		Lp order. 

	center : NumPy array type 
		A 2D array of shape (ds[0], ds[1]).
	
	"""
	out = (numpy.average(numpy.abs(input-center)**(power)))**(1.0/power)
	return out

def lp_pool_2d(input, powers, centers, ds, ignore_border=None, st=None, padding=(0, 0)):
	"""
	A convenience function for the DownSampleFactorLp Theano Op.

	Parameters
	----------
	* input : 4D Theano tensor of input
		Input to the Lp Pooling Layer. Has shape (N, C, H, W). Lp pooling will 
		be done over the last 2 dimensions. 

	* powers : 3D Theano tensor of Lp orders
		A 3D tensor of shape (C, pr, pc) of all the Lp orders for each Lp 
		pooling unit. 

	* centers : 5D Theano tensor of Lp centers
		A 5D tensor of shape (C, pr, pc, ds[0], ds[1]) of all the centers for 
		each Lp pooling unit.

	* ds : tuple of length 2
		Factor by which to downscale (vertical ds, horizontal ds).
		(2,2) will halve the image in each dimension.

	* ignore_border : bool (default None, will print a warning and set to False)
		When True, (5,5) input with ds=(2,2) will generate a (2,2) output.
		(3,3) otherwise.

	* st : tuple of two ints
		Stride size, which is the number of shifts over rows/cols to get the
		next pool region. If st is None, it is considered equal to ds
		(no overlap on pooling regions).

	* padding : tuple of two ints
		(pad_h, pad_w), pad zeros to extend beyond four borders
		of the images, pad_h is the size of the top and bottom margins,
		and pad_w is the size of the left and right margins.
	"""
	if input.ndim != 4:
		raise NotImplementedError('lp_pool_2d requires a dimension = 4')
	if ignore_border is None:
		warnings.warn(
			"lp_pool_2d() will have the parameter ignore_border"
			" default value changed to True (currently"
			" False). To have consistent behavior with all Theano"
			" version, explicitly add the parameter ignore_border=True."
			" On the GPU, using ignore_border=False is needed to use CuDNN."
			" When using ignore_border=False and not using CuDNN, the only"
			" GPU combination supported is when"
			" `ds == st and padding == (0, 0)."
			" Otherwise, the convolution will be executed on CPU.",
			stacklevel=2)
		ignore_border = False
	if input.ndim == 4:
		op = DownsampleFactorLp(ds, ignore_border, st=st, padding=padding)
		output = op(input, powers, centers)
		return output

	########################################################
	##### 2/10/16: why do we do this whole reshaping ? #####
	########################################################

	# Extract input dimensions
	img_shape = input.shape[-2:]

	# Count the number of "leading" dimensions, store as dmatrix
	batch_size = tensor.prod(input.shape[:-2])
	batch_size = tensor.shape_padright(batch_size, 1)

	# Store as 4D tensor with shape: (batch_size,1,height,width)
	new_shape = tensor.cast(tensor.join(0, batch_size,
										tensor.as_tensor([1]),
										img_shape), 'int64')
	input_4D = tensor.reshape(input, new_shape, ndim=4)

	# Perform the Lp norm pooling
	op = DownsampleFactorLp(ds, ignore_border, st=st, padding=padding)
	output = op(input_4D, powers, centers)

	# Restore to original shape
	outshp = tensor.join(0, input.shape[:-2], output.shape[-2:])
	return tensor.reshape(output, outshp, ndim=input.ndim)
	###########################################################################

class DownsampleFactorLp(Op):
	""" 
	A Theano Op which performs Lp pooling as follows: for 4D input with 
	shape (N, C, H, W), this function returns a 4D output of shape 
	(N, C, pr, pc) 
	
	where:
		- pr = 1 + (H-ds[0]+padding[0])/st[0] 
		- pc = 1 + (W-ds[1]+padding[1])/st[1]

	Let x = input[n, k, xr:xr+ds[0], xc:xc+ds[1]] denote any valid spatial 
	pooling region in the input. Let the spatial pooling region given by	
	(xr:xr+ds[0], xc:xc+ds[1]) be indexed by (m,n) index where:

				 	0 <= m,n < pr, pc, respectively, 

	i.e. we assign it its corresponding spatial index in the output tensor. We 
	then compute the activation value z of this pooling region using the 
	following formula:

	z = ((1/(ds[0]*ds[1]))\sum_{i = 0}^{ds[0]-1}\sum_{j=0}^{ds[1]-1} |x[n,k,i,j]-c[i,j]|^(p)^(1/p)
	
	where:
		- p = powers[k,m,n] is the Lp order corresponding to the (k,m,n) 
			  pooling unit 
		- c = centers[k,m,n] is the (ds[0], ds[1]) shaped array which 
			  represents the center of the channel (k,m,n) Lp pooling unit	

	Parameters
	----------
	* ds : list or tuple of two ints
		Downsample factor over rows and column.
		ds indicates the pool region size.

	* ignore_border : bool
		If ds doesn't divide imgshape, do we include an extra row/col of 
		partial downsampling (False) or ignore it (True).

	* st : list or tuple of two ints or None
		Stride size, which is the number of shifts over rows/cols to get the
		next pool region. If st is None, it is considered equal to ds
		(no overlap on pooling regions).

	* padding: tuple of two ints
		(pad_h, pad_w), pad zeros to extend beyond four borders of the images,
		pad_h is the size of the top and bottom margins, and pad_w is the size
		of the left and right margins.
	"""

	__props__ = ('ds', 'ignore_border', 'st', 'padding')

	@staticmethod
	def out_shape(imgshape, ds, ignore_border=False, st=None, padding=(0, 0)):
		"""
		Return the shape of the output from this op, for input of given
		shape and flags.

		Parameters
		----------
		* imgshape : tuple, list, or similar of integer or scalar Theano variable
			The shape of a tensor of images. The last two elements are
			interpreted as the number of rows, and the number of cols.

		* ds : list or tuple of two ints
			Downsample factor over rows and columns this parameter indicates
			the size of the pooling region.

		* st : list or tuple of two ints
			The stride size. This is the distance between the pooling regions.
			If it's set to None, it equals ds.

		* ignore_border : bool
			If ds doesn't divide imgshape, do we include an extra row/col of
			partial downsampling (False) or ignore it (True).

		* padding : tuple of two ints
			(pad_h, pad_w), pad zeros to extend beyond four borders
			of the images, pad_h is the size of the top and bottom margins,
			and pad_w is the size of the left and right margins.

		Returns
		-------
		list
			The shape of the output from this op, for input of given shape.
			This will have the same length as imgshape, but with last two
			elements reduced as per the downsampling & ignore_border flags.

		"""
		if len(imgshape) < 2:
			raise TypeError('imgshape must have at least two elements '
							'(rows, cols)')

		if st is None:
			st = ds
		r, c = imgshape[-2:]
		r += padding[0] * 2
		c += padding[1] * 2

		if ignore_border:
			if ds[0] == st[0]:
				nr = r // st[0] # Python 3, "//" does integer division
			else:
				out_r = (r - ds[0]) // st[0] + 1
				if isinstance(r, theano.Variable):
					nr = tensor.maximum(out_r, 0)
				else:
					nr = numpy.maximum(out_r, 0)

			if ds[1] == st[1]:
				nc = c // st[1]
			else:
				out_c = (c - ds[1]) // st[1] + 1
				if isinstance(c, theano.Variable):
					nc = tensor.maximum(out_c, 0)
				else:
					nc = numpy.maximum(out_c, 0)
		else:
			if isinstance(r, theano.Variable):
				nr = tensor.switch(tensor.ge(st[0], ds[0]),
								   (r - 1) // st[0] + 1,
								   tensor.maximum(0, (r - 1 - ds[0]) //
												  st[0] + 1) + 1)
			elif st[0] >= ds[0]:
				nr = (r - 1) // st[0] + 1
			else:
				nr = max(0, (r - 1 - ds[0]) // st[0] + 1) + 1

			if isinstance(c, theano.Variable):
				nc = tensor.switch(tensor.ge(st[1], ds[1]),
								   (c - 1) // st[1] + 1,
								   tensor.maximum(0, (c - 1 - ds[1]) //
												  st[1] + 1) + 1)
			elif st[1] >= ds[1]:
				nc = (c - 1) // st[1] + 1
			else:
				nc = max(0, (c - 1 - ds[1]) // st[1] + 1) + 1

		rval = list(imgshape[:-2]) + [nr, nc]
		return rval

	def __init__(self, ds, ignore_border=False, st=None, padding=(0, 0)):
		""" 
		Initializes the Lp downsampling Op. 

		"""
		self.ds = tuple(ds)
		if not all([isinstance(d, int) for d in ds]):
			raise ValueError(
				"DownsampleFactorLp downsample parameters must be ints."
				" Got %s" % str(ds))
		if st is None:
			st = ds
		assert isinstance(st, (tuple, list))
		self.st = tuple(st)
		self.ignore_border = ignore_border
		self.padding = tuple(padding)
		if self.padding != (0, 0) and not ignore_border:
			raise NotImplementedError(
				'padding works only with ignore_border=True')
		if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
			raise NotImplementedError(
				'padding_h and padding_w must be smaller than strides')

	def make_node(self, x, powers, centers):
		""" 
		Creates a node. 
		"""
		if x.type.ndim != 4:
			raise TypeError()
		# TODO: consider restricting the dtype?
		x = tensor.as_tensor_variable(x)
		powers = tensor.as_tensor_variable(powers)
		centers = tensor.as_tensor_variable(centers)
		# If the input shape are broadcastable we can have 0 in the output shape
		broad = x.broadcastable[:2] + (False, False)
		out = tensor.TensorType(x.dtype, broad)
		return gof.Apply(self, [x, powers, centers], [out()])

	def perform(self, node, inputs, output_storage):
		""" Python code, primarily using NumPy, that computes the Lp downsampling.
		"""
		x, powers, centers = inputs[0], inputs[1], inputs[2]
		z = output_storage[0]
		if len(x.shape) != 4:
			raise NotImplementedError(
				'DownsampleFactorMax requires 4D input')
		z_shape = self.out_shape(x.shape, self.ds, self.ignore_border, self.st,
								 self.padding)
		if (z[0] is None) or (z[0].shape != z_shape):
			z[0] = numpy.empty(z_shape, dtype=x.dtype)
		zz = z[0]
		# Number of pooling output rows
		pr = zz.shape[-2]
		# Number of pooling output cols
		pc = zz.shape[-1]
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pad_h = self.padding[0]
		pad_w = self.padding[1]
		img_rows = x.shape[-2] + 2 * pad_h
		img_cols = x.shape[-1] + 2 * pad_w
		# 2/10/16: I want to add an option where we include or exclude the padding. This will involve padding of the center? So the only thing to account for is the different averaging as we will get zeros from the padding. But we'll focus on this later. 
		# inc_pad = self.mode == 'average_inc_pad'

		# Pad the image
		if self.padding != (0, 0):
			y = numpy.zeros(
				(x.shape[0], x.shape[1], img_rows, img_cols),
				dtype=x.dtype)
			y[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)] = x
		else:
			y = x

		# Define the function which will actually perform all the Lp spatial 
		# pooling operations
		func = lp_pool_func

		# Initialize the Lp orders
		powers = 1 + numpy.log(1+numpy.exp(powers))

		for n in xrange(x.shape[0]):
			for k in xrange(x.shape[1]):
				for r in xrange(pr):
					row_st = r * st0
					row_end = builtins.min(row_st + ds0, img_rows)
					# if not inc_pad:
					# 	row_st = builtins.max(row_st, self.padding[0])
					# 	row_end = builtins.min(row_end, x.shape[-2] + pad_h)
					for c in xrange(pc):
						col_st = c * st1
						col_end = builtins.min(col_st + ds1, img_cols)
						# if not inc_pad:
						# 	col_st = builtins.max(col_st, self.padding[1])
						# 	col_end = builtins.min(col_end,
						# 						   x.shape[-1] + pad_w)
						zz[n, k, r, c] = func(y[n, k, row_st:row_end, col_st:col_end], powers[k,r,c], centers[k,r,c]) # Lp pool with numpy W
		z[0] = zz

	def infer_shape(self, node, in_shapes):
		shp = self.out_shape(in_shapes[0], self.ds,
							 self.ignore_border, self.st, self.padding)
		return [shp]

	def grad(self, inp, grads):
		"""
		Computes the backpropagation of errors for the input, the Lp powers and the Lp centers.

		"""
		# x, powers, centers = inp[0], inp[1], inp[2]
		x, powers, centers = inp
		# gz = grads # gz is a list if I do it this way. When using the Lasagne Lp Layer, this will cause a segmentation fault for C-Code, or a Value Error for the Python code since gz is 5D that needs to be squeezed/flattened to get the true underlyign 4D tensor. 
		gz = grads[0] # gz is the Theano tensor variable now
		lp_out = self(x, powers, centers) # Compute the lp pooling forward pass

		return [LpPoolGrad_x(	self.ds,
								ignore_border=self.ignore_border,	
								st=self.st, 
								padding=self.padding)(x, lp_out, powers, centers, gz),
				LpPoolGrad_p(	self.ds,
								ignore_border=self.ignore_border,	
								st=self.st, 
								padding=self.padding)(x, lp_out, powers, centers, gz),
				LpPoolGrad_c(	self.ds,
								ignore_border=self.ignore_border,	
								st=self.st, 
								padding=self.padding)(x, lp_out, powers, centers, gz)]

	def c_code(self, node, name, inp, out, sub):
		""" 
		C code which performs the Lp downsampling. 
		"""
		x, powers, centers = inp
		z, = out
		fail = sub['fail']
		ignore_border = int(self.ignore_border)
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		ccode = """
		int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
		int pool_size = %(ds0)s * %(ds1)s;
		int z_r, z_c; // shape of the output
		int r, c; // shape of the padded_input
		if(PyArray_NDIM(%(x)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "x must be a 4D ndarray");
			%(fail)s;
		}
		r = PyArray_DIMS(%(x)s)[2];
		c = PyArray_DIMS(%(x)s)[3];
		r += %(pd0)s * 2;
		c += %(pd1)s * 2;

		if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
			{
			  PyErr_SetString(PyExc_ValueError,
				"padding must be (0,0) when ignore border is False");
			  %(fail)s;
			}
		if (%(ignore_border)s)
		{

			// '/' in C is different from '/' in python
			if (r - %(ds0)s < 0)
			{
			  z_r = 0;
			}
			else
			{
			  z_r = (r - %(ds0)s) / %(st0)s + 1;
			}
			if (c - %(ds1)s < 0)
			{
			  z_c = 0;
			}
			else
			{
			  z_c = (c - %(ds1)s) / %(st1)s + 1;
			}
		}
		else
		{
			// decide how many rows the output has
			if (%(st0)s >= %(ds0)s)
			{
				z_r = (r - 1) / %(st0)s + 1;
			}
			else
			{
				z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
			}
			// decide how many columns the output has
			if (%(st1)s >= %(ds1)s)
			{
				z_c = (c - 1) / %(st1)s + 1;
			}
			else
			{
				z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
			}
		}
		// memory allocation of z if necessary
		if ((!%(z)s)
		  || *PyArray_DIMS(%(z)s)!=4
		  ||(PyArray_DIMS(%(z)s)[0] != PyArray_DIMS(%(x)s)[0])
		  ||(PyArray_DIMS(%(z)s)[1] != PyArray_DIMS(%(x)s)[1])
		  ||(PyArray_DIMS(%(z)s)[2] != z_r)
		  ||(PyArray_DIMS(%(z)s)[3] != z_c)
		  )
		{
		  if (%(z)s) Py_XDECREF(%(z)s);
		  npy_intp dims[4] = {0,0,0,0};
		  dims[0]=PyArray_DIMS(%(x)s)[0];
		  dims[1]=PyArray_DIMS(%(x)s)[1];
		  dims[2]=z_r;
		  dims[3]=z_c;
		  //TODO: zeros not necessary
		  %(z)s = (PyArrayObject*) PyArray_ZEROS(4, dims, typenum,0);
		}

		// used for indexing a pool region inside the input
		int r_st, r_end, c_st, c_end;
		dtype_%(x)s collector; // temp var for the value in a region
		if (z_r && z_c)
		{
			for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
			  for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
				for(int i=0; i< z_r; i++){
				  r_st = i * %(st0)s;
				  r_end = r_st + %(ds0)s;
				  // skip the padding
				  r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
				  r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
				  // from padded_img space to img space
				  r_st -= %(pd0)s;
				  r_end -= %(pd0)s;

				  // handle the case where no padding, ignore border is True
				  if (%(ignore_border)s)
				  {
					r_end = r_end > r ? r : r_end;
				  }
				  for(int j=0; j<z_c; j++){
					c_st = j * %(st1)s;
					c_end = c_st + %(ds1)s;
					// skip the padding
					c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
					c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
					dtype_%(z)s * z = (
						  (dtype_%(z)s*)(PyArray_GETPTR4(%(z)s, b, k, i, j)));
					// change coordinates from padding_img space into img space
					c_st -= %(pd1)s;
					c_end -= %(pd1)s;
					// handle the case where no padding, ignore border is True
					if (%(ignore_border)s)
					{
					  c_end = c_end > c ? c : c_end;
					}
		"""
		ccode += """
					collector = 0.0f;

					// Get the lp_power for the current lp pooling unit: 
					// b is the batch index, k is the channel index, i, j are the spatial pooling indices 
					dtype_%(powers)s p = ((dtype_%(powers)s*)(PyArray_GETPTR3(%(powers)s,k,i,j)))[0];
					p = 1 + log(1 + exp(p));

					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
					  	
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];

						// Get the Lp pooling units center location:
						npy_intp center_dims[5] = {k, i, j, m-r_st, n-c_st};
						dtype_%(centers)s c = ((dtype_%(centers)s*)(PyArray_GetPtr(%(centers)s, center_dims)))[0];
						
						 collector += powf(fabs(a-c), p);
					  }
					}

					// Need to do the average, i.e. collector /= (pool_size)
					collector = collector / ((float) (pool_size));
					z[0] = powf(collector, 1.0/p);
			"""
		ccode += """
				  }
				}
			  }
			}
		}
		"""
		return ccode % locals()

class LpPoolGrad_x(Op):
	""" Theano Op which performs the backpropagation of gradients for the input x, returns gx.

	"""
	__props__ = ('ds', 'ignore_border', 'st', 'padding')

	def __init__(self, ds, ignore_border, st=None, padding=(0, 0), mode='max'):
		self.ds = tuple(ds)
		if not all([isinstance(d, int) for d in ds]):
			raise ValueError(
				"DownsampleFactorMax downsample parameters must be ints."
				" Got %s" % str(ds))
		if st is None:
			st = ds
		assert isinstance(st, (tuple, list))
		self.st = tuple(st)
		self.ignore_border = ignore_border
		self.padding = tuple(padding)
		if self.padding != (0, 0) and not ignore_border:
			raise NotImplementedError(
				'padding works only with ignore_border=True')
		if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
			raise NotImplementedError(
				'padding_h and padding_w must be smaller than strides')

	def make_node(self, x, lp_out, powers, centers, gz):
		x = tensor.as_tensor_variable(x)
		lp_out = tensor.as_tensor_variable(lp_out)
		powers = tensor.as_tensor_variable(powers)
		centers = tensor.as_tensor_variable(centers)
		gz = tensor.as_tensor_variable(gz)
		return gof.Apply(self, [x, lp_out, powers, centers, gz], [x.type()])

	def perform(self, node, inp, out):
		x, lp_out, powers, centers, gz = inp[0], inp[1], inp[2], inp[3], inp[4]
		gx_stg = out[0]
		# Number of pooling output rows
		pr = lp_out.shape[-2]
		# Number of pooling output cols
		pc = lp_out.shape[-1]
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pad_h = self.padding[0]
		pad_w = self.padding[1]
		img_rows = x.shape[-2] + 2 * pad_h
		img_cols = x.shape[-1] + 2 * pad_w

		powers = 1 + numpy.log(1+numpy.exp(powers))

		# Pad the image
		if self.padding != (0, 0):
			y = numpy.zeros(
				(x.shape[0], x.shape[1], img_rows, img_cols),
				dtype=x.dtype)
			y[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)] = x
		else:
			y = x
		gx = numpy.zeros_like(y)
		for n in xrange(x.shape[0]):
			for k in xrange(x.shape[1]):
				for r in xrange(pr):
					row_st = builtins.max(r * st0, self.padding[0])
					row_end = builtins.min(row_st + ds0, img_rows)
					for c in xrange(pc):
						col_st = builtins.max(c * st1, self.padding[1])
						col_end = builtins.min(col_st + ds1, img_cols)
						for row_ind in xrange(row_st, row_end):
							for col_ind in xrange(col_st, col_end):
								# print gz.shape
								gx[n, k, row_ind, col_ind] += ((1.0/(ds0*ds1))*lp_out[n, k, r, c]**(powers[k,r,c]-1)*numpy.sign(x[n,k,row_ind,col_ind]-centers[k,r,c,row_ind-row_st,col_ind-col_st])*numpy.abs(x[n,k,row_ind, col_ind]-centers[k,r,c,row_ind-row_st,col_ind-col_st])**(powers[k,r,c]-1))*gz[n,k,r,c]
		# Unpad the image
		gx = gx[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)]
		gx_stg[0] = gx

	def c_code(self, node, name, inp, out, sub):
		x, lp_out, powers, centers, gz, = inp
		gx, = out
		fail = sub['fail']
		ignore_border = int(self.ignore_border)
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		ccode = """
		int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
		int pool_size = %(ds0)s * %(ds1)s;
		int z_r, z_c; // shape of the output
		int r, c; // shape of the padded_input
		if(PyArray_NDIM(%(x)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "x must be a 4D ndarray");
			%(fail)s;
		}
		r = PyArray_DIMS(%(x)s)[2];
		c = PyArray_DIMS(%(x)s)[3];
		r += %(pd0)s * 2;
		c += %(pd1)s * 2;

		if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
			{
			  PyErr_SetString(PyExc_ValueError,
				"padding must be (0,0) when ignore border is False");
			  %(fail)s;
			}
		if (%(ignore_border)s)
		{

			// '/' in C is different from '/' in python
			if (r - %(ds0)s < 0)
			{
			  z_r = 0;
			}
			else
			{
			  z_r = (r - %(ds0)s) / %(st0)s + 1;
			}
			if (c - %(ds1)s < 0)
			{
			  z_c = 0;
			}
			else
			{
			  z_c = (c - %(ds1)s) / %(st1)s + 1;
			}
		}
		else
		{
			// decide how many rows the output has
			if (%(st0)s >= %(ds0)s)
			{
				z_r = (r - 1) / %(st0)s + 1;
			}
			else
			{
				z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
			}
			// decide how many columns the output has
			if (%(st1)s >= %(ds1)s)
			{
				z_c = (c - 1) / %(st1)s + 1;
			}
			else
			{
				z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
			}
		}
		// memory allocation of gx if necessary
		if ((!%(gx)s)
		  || *PyArray_DIMS(%(gx)s)!=4
		  ||(PyArray_DIMS(%(gx)s)[0] != PyArray_DIMS(%(x)s)[0])
		  ||(PyArray_DIMS(%(gx)s)[1] != PyArray_DIMS(%(x)s)[1])
		  ||(PyArray_DIMS(%(gx)s)[2] != PyArray_DIMS(%(x)s)[2])
		  ||(PyArray_DIMS(%(gx)s)[3] != PyArray_DIMS(%(x)s)[3])
		  )
		{
		  if (%(gx)s) Py_XDECREF(%(gx)s);
		  npy_intp dims[4] = {0,0,0,0};
		  dims[0]=PyArray_DIMS(%(x)s)[0];
		  dims[1]=PyArray_DIMS(%(x)s)[1];
		  dims[2]=PyArray_DIMS(%(x)s)[2];
		  dims[3]=PyArray_DIMS(%(x)s)[3];
		  //TODO: zeros not necessary
		  %(gx)s = (PyArrayObject*) PyArray_ZEROS(4, dims, typenum,0);
		}

		// used for indexing a pool region inside the input
		int r_st, r_end, c_st, c_end;
		dtype_%(x)s collector; // temp var for the value in a region
		if (z_r && z_c)
		{
			for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
			  for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
				for(int i=0; i< z_r; i++){
				  r_st = i * %(st0)s;
				  r_end = r_st + %(ds0)s;
				  // skip the padding
				  r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
				  r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
				  // from padded_img space to img space
				  r_st -= %(pd0)s;
				  r_end -= %(pd0)s;

				  // handle the case where no padding, ignore border is True
				  if (%(ignore_border)s)
				  {
					r_end = r_end > r ? r : r_end;
				  }
				  for(int j=0; j<z_c; j++){
					c_st = j * %(st1)s;
					c_end = c_st + %(ds1)s;
					// skip the padding
					c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
					c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
					// change coordinates from padding_img space into img space
					c_st -= %(pd1)s;
					c_end -= %(pd1)s;
					// handle the case where no padding, ignore border is True
					if (%(ignore_border)s)
					{
					  c_end = c_end > c ? c : c_end;
					}
		"""
		ccode += """
					collector = 0.0f;

					// Get the lp_power for the current lp pooling unit: 
					// b is the batch index, k is the channel index, i, j are the spatial pooling indices 
					dtype_%(powers)s p = ((dtype_%(powers)s*)(PyArray_GETPTR3(%(powers)s,k,i,j)))[0];
					p = 1 + log(1 + exp(p));

					// Get the lp_out activation value for the current Lp pooling unit:
					dtype_%(lp_out)s y = ((dtype_%(lp_out)s*)(PyArray_GETPTR4(%(lp_out)s,b,k,i,j)))[0];

					// Get the gradient gz at the current Lp pooling unit:
					dtype_%(gz)s gz = ((dtype_%(gz)s*)(PyArray_GETPTR4(%(gz)s,b,k,i,j)))[0];

					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
					  	
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];

						// Get the gx term to update:
						dtype_%(gx)s* gx = ((dtype_%(gx)s*)(PyArray_GETPTR4(%(gx)s,b,k,m,n)));

						// Get the Lp pooling units center location:
						npy_intp center_dims[5] = {k, i, j, m-r_st, n-c_st};
						dtype_%(centers)s c = ((dtype_%(centers)s*)(PyArray_GetPtr(%(centers)s, center_dims)))[0];

						// I should have all the pieces to perform the gx backprop computation! This may or may not be the most efficient way, and we have a long way to go it seems. 
						
						gx[0] += (1.0/((float) (pool_size)))*powf(y,p-1)*copysignf(1.0, a-c)*powf(fabs(a-c),p-1)*gz;
					  }
					}
			"""
		ccode += """
				  }
				}
			  }
			}
		}
		"""
		return ccode % locals()

class LpPoolGrad_c(Op):

	__props__ = ('ds', 'ignore_border', 'st', 'padding')

	def __init__(self, ds, ignore_border, st=None, padding=(0, 0), mode='max'):
		self.ds = tuple(ds)
		if not all([isinstance(d, int) for d in ds]):
			raise ValueError(
				"DownsampleFactorMax downsample parameters must be ints."
				" Got %s" % str(ds))
		if st is None:
			st = ds
		assert isinstance(st, (tuple, list))
		self.st = tuple(st)
		self.ignore_border = ignore_border
		self.padding = tuple(padding)
		if self.padding != (0, 0) and not ignore_border:
			raise NotImplementedError(
				'padding works only with ignore_border=True')
		if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
			raise NotImplementedError(
				'padding_h and padding_w must be smaller than strides')

	def make_node(self, x, lp_out, powers, centers, gz):
		x = tensor.as_tensor_variable(x)
		lp_out = tensor.as_tensor_variable(lp_out)
		powers = tensor.as_tensor_variable(powers)
		centers = tensor.as_tensor_variable(centers)
		gz = tensor.as_tensor_variable(gz)
		return gof.Apply(self, [x, lp_out, powers, centers, gz], [centers.type()])

	def perform(self, node, inp, out):
		x, lp_out, powers, centers, gz = inp[0], inp[1], inp[2], inp[3], inp[4]
		gc_stg = out[0]
		# Number of pooling output rows
		pr = lp_out.shape[-2]
		# Number of pooling output cols
		pc = lp_out.shape[-1]
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pad_h = self.padding[0]
		pad_w = self.padding[1]
		img_rows = x.shape[-2] + 2 * pad_h
		img_cols = x.shape[-1] + 2 * pad_w

		powers = 1 + numpy.log(1+numpy.exp(powers))

		# Pad the image
		if self.padding != (0, 0):
			y = numpy.zeros(
				(x.shape[0], x.shape[1], img_rows, img_cols),
				dtype=x.dtype)
			y[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)] = x
		else:
			y = x
		gc = numpy.zeros_like(centers)

		for n in xrange(x.shape[0]):
			for k in xrange(x.shape[1]):
				for r in xrange(pr):
					row_st = builtins.max(r * st0, self.padding[0])
					row_end = builtins.min(row_st + ds0, img_rows)
					for c in xrange(pc):
						col_st = builtins.max(c * st1, self.padding[1])
						col_end = builtins.min(col_st + ds1, img_cols)
						for row_ind in xrange(row_st, row_end):
							for col_ind in xrange(col_st, col_end):
								# print row_ind-row_st, col_ind-col_st
								gc[k, r, c, row_ind-row_st, col_ind-col_st] += ((1.0/(ds0*ds1))*lp_out[n, k, r, c]**(powers[k,r,c]-1)*(-1)*numpy.sign(y[n,k,row_ind, col_ind]-centers[k,r,c,row_ind-row_st,col_ind-col_st])*numpy.abs(y[n,k,row_ind, col_ind]-centers[k,r,c,row_ind-row_st,col_ind-col_st])**(powers[k,r,c]-1))*gz[n,k,r,c]

		gc_stg[0] = gc


	# def infer_shape(self, node, in_shapes):
	# 	return [in_shapes[1]]

	def c_code(self, node, name, inp, out, sub):
		x, lp_out, powers, centers, gz, = inp
		gc, = out
		fail = sub['fail']
		ignore_border = int(self.ignore_border)
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		ccode = """
		int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
		int pool_size = %(ds0)s * %(ds1)s;
		int z_r, z_c; // shape of the output
		int r, c; // shape of the padded_input
		if(PyArray_NDIM(%(x)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "x must be a 4D ndarray");
			%(fail)s;
		}
		r = PyArray_DIMS(%(x)s)[2];
		c = PyArray_DIMS(%(x)s)[3];
		r += %(pd0)s * 2;
		c += %(pd1)s * 2;

		if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
			{
			  PyErr_SetString(PyExc_ValueError,
				"padding must be (0,0) when ignore border is False");
			  %(fail)s;
			}
		if (%(ignore_border)s)
		{

			// '/' in C is different from '/' in python
			if (r - %(ds0)s < 0)
			{
			  z_r = 0;
			}
			else
			{
			  z_r = (r - %(ds0)s) / %(st0)s + 1;
			}
			if (c - %(ds1)s < 0)
			{
			  z_c = 0;
			}
			else
			{
			  z_c = (c - %(ds1)s) / %(st1)s + 1;
			}
		}
		else
		{
			// decide how many rows the output has
			if (%(st0)s >= %(ds0)s)
			{
				z_r = (r - 1) / %(st0)s + 1;
			}
			else
			{
				z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
			}
			// decide how many columns the output has
			if (%(st1)s >= %(ds1)s)
			{
				z_c = (c - 1) / %(st1)s + 1;
			}
			else
			{
				z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
			}
		}
		// memory allocation of gc if necessary
		if ((!%(gc)s)
		  || *PyArray_DIMS(%(gc)s)!=5
		  ||(PyArray_DIMS(%(gc)s)[0] != PyArray_DIMS(%(centers)s)[0])
		  ||(PyArray_DIMS(%(gc)s)[1] != PyArray_DIMS(%(centers)s)[1])
		  ||(PyArray_DIMS(%(gc)s)[2] != PyArray_DIMS(%(centers)s)[2])
		  ||(PyArray_DIMS(%(gc)s)[3] != PyArray_DIMS(%(centers)s)[3])
		  ||(PyArray_DIMS(%(gc)s)[4] != PyArray_DIMS(%(centers)s)[4])
		  )
		{
		  if (%(gc)s) Py_XDECREF(%(gc)s);
		  npy_intp dims[5] = {0,0,0,0,0};
		  dims[0]=PyArray_DIMS(%(centers)s)[0];
		  dims[1]=PyArray_DIMS(%(centers)s)[1];
		  dims[2]=PyArray_DIMS(%(centers)s)[2];
		  dims[3]=PyArray_DIMS(%(centers)s)[3];
		  dims[4]=PyArray_DIMS(%(centers)s)[4];
		  //TODO: zeros not necessary
		  %(gc)s = (PyArrayObject*) PyArray_ZEROS(5, dims, typenum,0);
		}

		// used for indexing a pool region inside the input
		int r_st, r_end, c_st, c_end;
		// dtype_%(x)s collector; // temp var for the value in a region
		if (z_r && z_c)
		{
			for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
			  for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
				for(int i=0; i< z_r; i++){
				  r_st = i * %(st0)s;
				  r_end = r_st + %(ds0)s;
				  // skip the padding
				  r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
				  r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
				  // from padded_img space to img space
				  r_st -= %(pd0)s;
				  r_end -= %(pd0)s;

				  // handle the case where no padding, ignore border is True
				  if (%(ignore_border)s)
				  {
					r_end = r_end > r ? r : r_end;
				  }
				  for(int j=0; j<z_c; j++){
					c_st = j * %(st1)s;
					c_end = c_st + %(ds1)s;
					// skip the padding
					c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
					c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
					// change coordinates from padding_img space into img space
					c_st -= %(pd1)s;
					c_end -= %(pd1)s;
					// handle the case where no padding, ignore border is True
					if (%(ignore_border)s)
					{
					  c_end = c_end > c ? c : c_end;
					}
		"""
		ccode += """
					// Get the lp_power for the current lp pooling unit: 
					// b is the batch index, k is the channel index, i, j are the spatial pooling indices 
					dtype_%(powers)s p = ((dtype_%(powers)s*)(PyArray_GETPTR3(%(powers)s,k,i,j)))[0];
					p = 1 + log(1 + exp(p));

					// Get the lp_out activation value for the current Lp pooling unit:
					dtype_%(lp_out)s y = ((dtype_%(lp_out)s*)(PyArray_GETPTR4(%(lp_out)s,b,k,i,j)))[0];

					// Get the gradient gz at the current Lp pooling unit:
					dtype_%(gz)s gz = ((dtype_%(gz)s*)(PyArray_GETPTR4(%(gz)s,b,k,i,j)))[0];

					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
					  	
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];

						// Get the centers and the gc term to update:
						npy_intp center_dims[5] = {k, i, j, m-r_st, n-c_st};
						dtype_%(centers)s c = ((dtype_%(centers)s*)(PyArray_GetPtr(%(centers)s, center_dims)))[0];
						dtype_%(gc)s* gc = ((dtype_%(gc)s*)(PyArray_GetPtr(%(gc)s, center_dims)));

						gc[0] += (1.0/((float) (pool_size)))*powf(y,p-1)*(-1.0f)*copysignf(1.0, a-c)*powf(fabs(a-c),p-1)*gz;
					  }
					}
			"""
		ccode += """
				  }
				}
			  }
			}
		}
		"""
		return ccode % locals()

class LpPoolGrad_p(Op):
	__props__ = ('ds', 'ignore_border', 'st', 'padding')

	def __init__(self, ds, ignore_border, st=None, padding=(0, 0), mode='max'):
		self.ds = tuple(ds)
		if not all([isinstance(d, int) for d in ds]):
			raise ValueError(
				"DownsampleFactorMax downsample parameters must be ints."
				" Got %s" % str(ds))
		if st is None:
			st = ds
		assert isinstance(st, (tuple, list))
		self.st = tuple(st)
		self.ignore_border = ignore_border
		self.padding = tuple(padding)
		if self.padding != (0, 0) and not ignore_border:
			raise NotImplementedError(
				'padding works only with ignore_border=True')
		if self.padding[0] >= self.ds[0] or self.padding[1] >= self.ds[1]:
			raise NotImplementedError(
				'padding_h and padding_w must be smaller than strides')

	def make_node(self, x, lp_out, powers, centers, gz):
		x = tensor.as_tensor_variable(x)
		lp_out = tensor.as_tensor_variable(lp_out)
		powers = tensor.as_tensor_variable(powers)
		centers = tensor.as_tensor_variable(centers)
		gz = tensor.as_tensor_variable(gz)
		return gof.Apply(self, [x, lp_out, powers, centers, gz], [powers.type()])

	def perform(self, node, inp, out):
		x, lp_out, powers, centers, gz = inp[0], inp[1], inp[2], inp[3], inp[4]
		# print type(gz)
		lp_pows = 1 + numpy.log(1+numpy.exp(powers))
		gp_stg = out[0]
		gp = numpy.zeros_like(lp_pows)


		# Number of pooling output rows
		pr = gz.shape[-2]
		# Number of pooling output cols
		pc = gz.shape[-1]
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		img_rows = x.shape[-2] + 2 * pd0
		img_cols = x.shape[-1] + 2 * pd1

		# Pad the image
		if self.padding != (0, 0):
			y = numpy.zeros(
				(x.shape[0], x.shape[1], img_rows, img_cols),
				dtype=x.dtype)
			y[:, :, pad_h:(img_rows - pad_h), pad_w:(img_cols - pad_w)] = x
		else:
			y = x

		for n in xrange(x.shape[0]):
			for k in xrange(x.shape[1]):
				for r in xrange(pr):
					row_st = builtins.max(r * st0, self.padding[0])
					row_end = builtins.min(row_st + ds0, img_rows)
					for c in xrange(pc):
						col_st = builtins.max(c * st1, self.padding[1])
						col_end = builtins.min(col_st + ds1, img_cols)

						a = numpy.sum(numpy.log(numpy.abs(y[n,k,row_st:row_end, col_st:col_end]-centers[k,r,c])) * numpy.abs(y[n,k,row_st:row_end, col_st:col_end]-centers[k,r,c])**(lp_pows[k,r,c]))

						b = numpy.sum(numpy.abs(y[n,k,row_st:row_end, col_st:col_end]-centers[k,r,c])**(lp_pows[k,r,c]))

						gp[k,r,c] += lp_out[n,k,r,c]*((-numpy.log(lp_out[n,k,r,c])/lp_pows[k,r,c]) + (1.0/lp_pows[k,r,c])*(a/b))*gz[n,k,r,c]*sigmoid(powers[k,r,c])

		gp_stg[0] = gp

	def c_code(self, node, name, inp, out, sub):
		x, lp_out, powers, centers, gz, = inp
		gp, = out
		fail = sub['fail']
		ignore_border = int(self.ignore_border)
		ds0, ds1 = self.ds
		st0, st1 = self.st
		pd0, pd1 = self.padding
		ccode = """
		int typenum = PyArray_ObjectType((PyObject*)%(x)s, 0);
		int pool_size = %(ds0)s * %(ds1)s;
		int z_r, z_c; // shape of the output
		int r, c; // shape of the padded_input
		if(PyArray_NDIM(%(x)s)!=4)
		{
			PyErr_SetString(PyExc_ValueError, "x must be a 4D ndarray");
			%(fail)s;
		}
		r = PyArray_DIMS(%(x)s)[2];
		c = PyArray_DIMS(%(x)s)[3];
		r += %(pd0)s * 2;
		c += %(pd1)s * 2;

		if (%(pd0)s != 0 && %(pd1)s != 0 && !%(ignore_border)s)
			{
			  PyErr_SetString(PyExc_ValueError,
				"padding must be (0,0) when ignore border is False");
			  %(fail)s;
			}
		if (%(ignore_border)s)
		{

			// '/' in C is different from '/' in python
			if (r - %(ds0)s < 0)
			{
			  z_r = 0;
			}
			else
			{
			  z_r = (r - %(ds0)s) / %(st0)s + 1;
			}
			if (c - %(ds1)s < 0)
			{
			  z_c = 0;
			}
			else
			{
			  z_c = (c - %(ds1)s) / %(st1)s + 1;
			}
		}
		else
		{
			// decide how many rows the output has
			if (%(st0)s >= %(ds0)s)
			{
				z_r = (r - 1) / %(st0)s + 1;
			}
			else
			{
				z_r = std::max(0, (r - 1 - %(ds0)s) / %(st0)s + 1) + 1;
			}
			// decide how many columns the output has
			if (%(st1)s >= %(ds1)s)
			{
				z_c = (c - 1) / %(st1)s + 1;
			}
			else
			{
				z_c = std::max(0, (c - 1 - %(ds1)s) / %(st1)s + 1) + 1;
			}
		}
		// memory allocation of gc if necessary
		if ((!%(gp)s)
		  || *PyArray_DIMS(%(gp)s)!=3
		  ||(PyArray_DIMS(%(gp)s)[0] != PyArray_DIMS(%(powers)s)[0])
		  ||(PyArray_DIMS(%(gp)s)[1] != PyArray_DIMS(%(powers)s)[1])
		  ||(PyArray_DIMS(%(gp)s)[2] != PyArray_DIMS(%(powers)s)[2])
		  )
		{
		  if (%(gp)s) Py_XDECREF(%(gp)s);
		  npy_intp dims[3] = {0,0,0};
		  dims[0]=PyArray_DIMS(%(powers)s)[0];
		  dims[1]=PyArray_DIMS(%(powers)s)[1];
		  dims[2]=PyArray_DIMS(%(powers)s)[2];
		  //TODO: zeros not necessary
		  %(gp)s = (PyArrayObject*) PyArray_ZEROS(3, dims, typenum,0);
		}

		// used for indexing a pool region inside the input
		int r_st, r_end, c_st, c_end;
		// dtype_%(x)s collector; // temp var for the value in a region
		if (z_r && z_c)
		{
			for(int b=0; b<PyArray_DIMS(%(x)s)[0]; b++){
			  for(int k=0; k<PyArray_DIMS(%(x)s)[1]; k++){
				for(int i=0; i< z_r; i++){
				  r_st = i * %(st0)s;
				  r_end = r_st + %(ds0)s;
				  // skip the padding
				  r_st = r_st < %(pd0)s ? %(pd0)s : r_st;
				  r_end = r_end > (r - %(pd0)s) ? r - %(pd0)s : r_end;
				  // from padded_img space to img space
				  r_st -= %(pd0)s;
				  r_end -= %(pd0)s;

				  // handle the case where no padding, ignore border is True
				  if (%(ignore_border)s)
				  {
					r_end = r_end > r ? r : r_end;
				  }
				  for(int j=0; j<z_c; j++){
					c_st = j * %(st1)s;
					c_end = c_st + %(ds1)s;
					// skip the padding
					c_st = c_st < %(pd1)s ? %(pd1)s : c_st;
					c_end = c_end > (c - %(pd1)s) ? c - %(pd1)s : c_end;
					// change coordinates from padding_img space into img space
					c_st -= %(pd1)s;
					c_end -= %(pd1)s;
					// handle the case where no padding, ignore border is True
					if (%(ignore_border)s)
					{
					  c_end = c_end > c ? c : c_end;
					}
		"""
		ccode += """
					// Get the lp_power for the current lp pooling unit: 
					// b is the batch index, k is the channel index, i, j are the spatial pooling indices 
					dtype_%(powers)s p = ((dtype_%(powers)s*)(PyArray_GETPTR3(%(powers)s,k,i,j)))[0];
					dtype_%(powers)s lp_pow;
					dtype_%(powers)s sigmoid;
					if(p <0){
						sigmoid = 1.0/(1.0 + exp(-p));
					}
					else {
						sigmoid = exp(p)/(1.0+exp(p));
					}
					lp_pow = 1 + log(1 + exp(p));

					// Get the lp_out activation value for the current Lp pooling unit:
					dtype_%(lp_out)s y = ((dtype_%(lp_out)s*)(PyArray_GETPTR4(%(lp_out)s,b,k,i,j)))[0];

					// Get the gradient gz at the current Lp pooling unit:
					dtype_%(gz)s gz = ((dtype_%(gz)s*)(PyArray_GETPTR4(%(gz)s,b,k,i,j)))[0];

					// Define collector_a and collector_b terms for the sums:
					dtype_%(gp)s collector_a; 
					dtype_%(gp)s collector_b;
					collector_a = 0.0f;
					collector_b = 0.0f;

					// Define the gp terms to update
					dtype_%(gp)s * gp = (
						  (dtype_%(gp)s*)(PyArray_GETPTR3(%(gp)s, k,i,j)));

					// go through the pooled region in the unpadded input
					for(int m=r_st; m<r_end; m++)
					{
					  for(int n=c_st; n<c_end; n++)
					  {
					  	
						dtype_%(x)s a = ((dtype_%(x)s*)(PyArray_GETPTR4(%(x)s,b,k,m,n)))[0];

						// Get the centers and the gc term to update:
						npy_intp center_dims[5] = {k, i, j, m-r_st, n-c_st};
						dtype_%(centers)s c = ((dtype_%(centers)s*)(PyArray_GetPtr(%(centers)s, center_dims)))[0];

						collector_a += log(fabs(a-c))*powf(fabs(a-c),lp_pow);
						collector_b += powf(fabs(a-c), lp_pow); 
					  }
					}
					gp[0] += y*(-log(y)/lp_pow + (1.0/lp_pow)*(collector_a/collector_b))*gz*sigmoid; 
			"""
		ccode += """
				  }
				}
			  }
			}
		}
		"""
		return ccode % locals()

if __name__ == '__main__':
	pass 

	import theano
	import theano.tensor as T
	import numpy as np
	np.set_printoptions(suppress=True)
	# Test the forward pass: 
	input_var = T.tensor4('input_var')
	lp_powers = T.tensor3('lp orders')
	dtensor5 = T.TensorType('float32', (False,)*5)
	lp_centers = dtensor5('lp_centers')

	op = DownsampleFactorLp(ds=(2,2), st=(2,2))

	lp_pooled = op(input_var, lp_powers, lp_centers)

	get_pooled = theano.function([input_var, lp_powers, lp_centers], lp_pooled)

	x = np.arange(64).reshape(2,2,4,4).astype(theano.config.floatX)
	print x
	powers = np.ones((8)).reshape(2,2,2).astype(theano.config.floatX)
	centers = np.ones((32)).reshape(2,2,2,2,2).astype(theano.config.floatX)

	x_pooled = get_pooled(x, powers, centers)

	print x_pooled

	# # 1/26/16: DownsampleFactorLp perform Python works just fine 

	# Testing DownsampleFactorLp grad 
	gz = T.tensor4('gz grad')
	backprops = op.grad(inp=(input_var, lp_powers, lp_centers), grads=(gz))
	get_backprop_grads = theano.function([input_var, lp_powers, lp_centers, gz], backprops)
	gzz = np.arange(16).reshape(2,2,2,2).astype(theano.config.floatX)
	gzz /= 10.0

	print gzz

	gx, gw, gc = get_backprop_grads(x, powers, centers, gzz)

	print gx 

	print gc



	print gw