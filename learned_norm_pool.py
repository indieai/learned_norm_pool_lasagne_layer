import lasagne, theano 
from lasagne.layers import Layer 
from lasagne.utils import as_tuple
from lasagne.layers.pool import pool_output_length
import numpy as np
from lpdownsample import *

class LearnedNormPoolingLayer2D(Layer):
	""" A Learned Norm Lp pooling layer with trainable Lp orders and centers.

	Reference
	---------
	* Learned-Norm Pooling for Deep Feedforward and Recurrent Neural Networks by Caglar Gulcehre, Kyunghyun Cho, Razvan Pascanu, Yoshua Bengio, arXiv: https://arxiv.org/abs/1311.1780. 

	Parameters
	----------

	* pool_size : tuple type 
		A 2-tuple describing the pool size. 

	* stride : list or tuple of two ints or None
		Stride size, which is the number of shifts over rows/cols to get the
		next pool region. If st is None, it is considered equal to ds
		(no overlap on pooling regions).

	* pad : tuple of two ints
		(pad_h, pad_w), pad zeros to extend beyond four borders
		of the images, pad_h is the size of the top and bottom margins,
		and pad_w is the size of the left and right margins.

	* ignore_border : bool (default None, will print a warning and set to False)
		When True, (5,5) input with ds=(2,2) will generate a (2,2) output.
		(3,3) otherwise.

	* lp_powers : float type 
		A float type indicating the values to initialize our Lp powers. Default is set to 2.0. 
		Note: Given lp_powers, the actual Lp order is computed by the following equation:
			Lp_powers = 1 + ln(1+exp(lp_powers))
			This convention guarantees that the Lp orders will be strictly positive thus we preserve the norm property of our pooling layers during gradient descent. 

	* lp_centers : float type 
		A float type indicating the values to initialize our Lp powers. Default is set to 0.0. 
	"""

	def __init__(self, incoming, pool_size, stride, pad, ignore_border, Lp_powers=lasagne.init.Constant(val=1.0), Lp_centers = lasagne.init.Constant(val=0.0), **kwargs):

		super(LearnedNormPoolingLayer2D, self).__init__(incoming, **kwargs)

		self.pool_size = as_tuple(pool_size, 2)

		if len(self.input_shape) != 4:
			raise ValueError("Tried to create a 2D pooling layer with "
							 "input shape %r. Expected 4 input dimensions "
							 "(batchsize, channels, 2 spatial dimensions)."
							 % (self.input_shape,))
			
		if stride is None:
			self.stride = self.pool_size
		else:
			self.stride = as_tuple(stride, 2)

		self.pad = as_tuple(pad, 2)

		self.ignore_border = ignore_border

		# Here's where my code is different, I want to incorporate weights
		self.num_channels = self.input_shape[1] # Number of channels 

		self.num_vert_pool_units = pool_output_length(self.input_shape[2],
											 pool_size=self.pool_size[0],
											 stride=self.stride[0],
											 pad=self.pad[0],
											 ignore_border=self.ignore_border,
											 )

		self.num_horiz_pool_units = pool_output_length(self.input_shape[3],
											 pool_size=self.pool_size[1],
											 stride=self.stride[1],
											 pad=self.pad[1],
											 ignore_border=self.ignore_border,
											 )

		self.num_units = self.num_vert_pool_units + self.num_horiz_pool_units
		self.lp_powers = self.add_param(Lp_powers, (self.num_channels, self.num_vert_pool_units, self.num_horiz_pool_units), name='Lp power parameters', trainable = True, regularizable = True)
		self.lp_centers = self.add_param(Lp_centers, (self.num_channels, self.num_vert_pool_units, self.num_horiz_pool_units, self.pool_size[0], self.pool_size[1]), name='Lp center parameters', trainable = True, regularizable = True)

	def get_output_shape_for(self, input_shape):
		output_shape = list(input_shape)
		output_shape[2] = self.num_vert_pool_units
		output_shape[3] = self.num_horiz_pool_units

		return tuple(output_shape)

	def get_output_for(self, input, **kwargs):
		pooled = lp_pool_2d(input=input,
							powers=self.lp_powers,
							centers=self.lp_centers,
							ds=self.pool_size,
							st=self.stride,
							ignore_border=self.ignore_border,
							padding=self.pad)
		return pooled


if __name__ == '__main__':
	pass 


	# How to use numpy strides

	# A= np.arange(64).reshape(2, 2, 4, 4).astype('int32')
	# We interpret this as A having 2 data points, each having 2 channels of shape 4x4
	# from numpy.lib.stride_tricks import as_strided
	# A.strides = (128, 64, 16, 4) # This tells us how many contiguous bytes to move if we want to increase 1 in the corresponding dimension. For example, if we want to traverse by 1 across the columns (we move across a row), we must move 1 element, so 4 bytes for int32 type. If we wish to traverse 1 row (so we move down a column), we must traverse 4 contiguous elements, so 4*4bytes = 16 bytes, and so on. 

	# Now we wish to compute B, the 2x2 strided, non-overlapping regions.
	# B = as_strided(A, shape = (2, 2, 2, 2, 2, 2), strides = (128, 64, 32, 8, 16, 4))
	# Interpret the shape of B. B will have 2 datapoints, with 2 channels, and a 2x2 grid of 2x2 elements from A.

	# Breakdown: the number of shape dimensions has to match the number of strides. So think of it this way, the stride represents how much we have to move contiguously in that direction to traverse +1 in that dimension. So if I'm in the first shape dimension, which here corresponds to the datapoint. Say I'm in the first datapoint's entry, wherever that may be. And I want to go the same index but for the 2nd data point. I would have to move 128 contiguous bytes! If I'm in a certain channel, and I want to move to the same point in the next channel, I would have to move 64 contiguous bytes, etc. 

	# Apparently you can't do this trick in Theano. Reference here in comments by david eickenberg: https://stackoverflow.com/questions/29225213/in-theano-making-the-matrix-of-slices-from-a-vector. 


	# def matrix_strided(X, pool_size):
	# 	n_data, n_channels = X.shape[:2]
	# 	n_rows, n_cols = X.shape[-2:]
	# 	p_rows, p_cols = pool_size
	# 	X_strided = np.zeros((n_data, n_channels, n_rows/p_rows, n_cols/p_cols, p_rows, p_cols))

	# 	for n in xrange(n_data):
	# 		for i in xrange(X_strided.shape[2]):
	# 			for j in xrange(X_strided.shape[3]):
	# 				X_strided[n,:,i,j] = X[n,:, i*p_rows:(i+1)*p_rows, j*p_cols:(j+1)*p_cols]

	# 	return X_strided

	# How do I do the above using Theano scans? 
	

	# Different question: how can I perform the parameterized Lp pooling in Theano? Basically just want to create a parameterized function...

	# class LpNormPoolLayer(object):
	# 	def __init__(self, ds, num_strides):
	# 		pr, pc = num_strides
	# 		self.ds_r = ds[0]
	# 		self.ds_c = ds[1]
	# 		self.pr = pr
	# 		self.pc = pc
	# 		self.W = 3 * np.random.rand(pr, pc)

	# 	def get_output_for(self, stride_id, input):
	# 		C, H, W = input.shape

	# 		out = np.zeros((C,))

	# 		for i in xrange(C):
	# 			out[i] = (np.sum(input[i]**(self.W[stride_id[0], stride_id[1]]))*(1.0/(H*W)))**(1.0/self.W[stride_id[0], stride_id[1]])

	# 		return out

	# Lp = LpNormPoolLayer( (2,2), (2,2) )
	# X = np.random.choice(20, 3*2*2).reshape(3, 2, 2)
	# stride_id = (0,1)
	# power = Lp.W[stride_id[0], stride_id[1]]
	# out = Lp.get_output_for(stride_id, X)
	# print out 

	# correct_out = np.zeros((3,))

	# correct_out[0] = (np.sum(X[0]**power) * (1.0/4))**(1.0/power)

	# correct_out = np.array([(np.sum(X[i]**power) * (1.0/4))**(1.0/power) for i in xrange(X.shape[0])])

	# print correct_out